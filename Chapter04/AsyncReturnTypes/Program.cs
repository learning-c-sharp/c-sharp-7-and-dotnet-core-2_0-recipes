﻿using System;
using System.Threading.Tasks;

namespace AsyncReturnTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        private static async Task<bool> getBoolOldWayAsync()
        {
            await Task.Delay(3000);
            return false;
        }

        private static async ValueTask<bool> getBoolAsync()
        {
            await Task.Delay(3000);
            return true;
        }
    }
}