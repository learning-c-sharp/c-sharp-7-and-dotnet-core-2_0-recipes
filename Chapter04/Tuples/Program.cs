﻿using System;

namespace Tuples
{
    class Program
    {
        static void Main(string[] args)
        {
            var car = getCar();
            Console.WriteLine($"Model: {car.Item1}");
            Console.WriteLine($"Price: {car.Item2}");
            Console.WriteLine($"Currency: {car.Item3}");

            var car2 = getCar2();
            Console.WriteLine($"Model: {car2.Model}");
            Console.WriteLine($"Price: {car2.Price}");
            Console.WriteLine($"Currency: {car2.Currency}");

            (string model, double price, string currency) = getCar();
            Console.WriteLine($"Model: {model}");
            Console.WriteLine($"Price: {price}");
            Console.WriteLine($"Currency: {currency}");
        }

        private static (string, double, string) getCar()
        {
            return ("Tesla Model S", 75000, "USD");
        }

        private static (string Model, double Price, string Currency) getCar2()
        {
            return ("Tesla Model S", 75000, "USD");
        }
    }
}