using System;

namespace ThrowExpressions
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        private IRun _runner;

        public Person(string firstName, string lastName, IRun runner)
        {
            FirstName = firstName;
            LastName = lastName;
            _runner = runner ?? throw new ArgumentNullException(nameof(runner));
        }

        public void Run(int distance)
        {
            _runner.Run(distance);
        }
    }
}