﻿using System;

namespace ThrowExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Person p = new Person("John", "Smith", null);
                p.Run(9);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine($"{e.ParamName} cannot be null");
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops something went wrong");
            }
        }
    }
}