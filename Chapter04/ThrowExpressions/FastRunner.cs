using System;

namespace ThrowExpressions
{
    public class FastRunner : IRun
    {
        public void Run(int distance)
        {
            Console.WriteLine($"Running fast for {distance} km");
        }
    }
}