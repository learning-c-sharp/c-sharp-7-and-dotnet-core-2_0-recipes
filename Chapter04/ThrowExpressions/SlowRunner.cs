using System;

namespace ThrowExpressions
{
    public class SlowRunner : IRun
    {
        public void Run(int distance)
        {
            Console.WriteLine($"Running slow for {distance} km");
        }
    }
}