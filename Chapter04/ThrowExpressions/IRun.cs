namespace ThrowExpressions
{
    public interface IRun
    {
        void Run(int distance);
    }
}