﻿using System;

namespace PatternMatching
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Student("John", "Doe", 24, StudentLevel.HighSchool);
            describePerson(p);
            Person e = new Employee("John", "Smith", 42, EmployeeType.Permanent);
            describePerson(e);
            describeEmployee(e);
        }

        private static void describeEmployee(Person p)
        {
            if (p is Employee e)
            {
                if (e.Type == EmployeeType.Hourly)
                {
                    Console.WriteLine($"{e} is an hourly employee");
                }
                else
                {
                    Console.WriteLine($"{e} is a permanent employee");
                }
            }
            else
            {
                Console.WriteLine($"{p} is not an employee");
            }
        }

        private static void describePerson(Person p)
        {
            switch (p)
            {
                case Student s when s.Level == StudentLevel.HighSchool:
                    Console.WriteLine($"{s} is a high school student");
                    break;
                case Student s when s.Level == StudentLevel.Undergrad:
                    Console.WriteLine($"{s} is a undergraduate student");
                    break;
                case Student s when s.Level == StudentLevel.Postgrad:
                    Console.WriteLine($"{s} is a postgraduate student");
                    break;
                case Employee e:
                    Console.WriteLine($"{e} is en employee");
                    break;
                case Athlete a:
                    Console.WriteLine($"{a} is en athlete");
                    break;
                case Person v:
                    Console.WriteLine($"{v} is just a person");
                    break;
                default:
                    throw new ArgumentException("p is not a person");
            }
        }
    }
}