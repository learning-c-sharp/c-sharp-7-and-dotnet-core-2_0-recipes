﻿using System;

namespace InlineOutVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum, diff;
            calculate(8, 2, out sum, out diff);
            Console.WriteLine($"Sum: {sum}, Diff: {diff}");

            // declaring variables inline
            calculate(8, 2, out int sum2, out int diff2);
            Console.WriteLine($"Sum: {sum2}, Diff: {diff2}");

            // declaring inline variables with var keyword
            calculate(8, 2, out var sum3, out var diff3);
            Console.WriteLine($"Sum: {sum3}, Diff: {diff3}");
        }

        private static void calculate(int a, int b, out int sum, out int diff)
        {
            sum = a + b;
            diff = a - b;
        }
    }
}