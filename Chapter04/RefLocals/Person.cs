namespace RefLocals
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        private int _age;

        public Person(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            _age = age;
        }

        public ref int GetAge()
        {
            return ref _age;
        }
    
        public override string ToString()
        {
            return $"{FirstName} {LastName} - {_age}";
        }
    }
}