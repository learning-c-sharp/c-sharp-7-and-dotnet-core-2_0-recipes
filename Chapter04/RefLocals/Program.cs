﻿using System;

namespace RefLocals
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("John", "Doe", 42);
            Console.WriteLine(p);
            int age = p.GetAge();
            ref int ageRef = ref p.GetAge();
            age++;
            Console.WriteLine(p);
            ageRef++;
            Console.WriteLine(p);
        }
    }
}