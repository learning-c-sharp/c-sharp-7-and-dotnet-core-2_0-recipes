using System;

namespace LocalFunctions
{
    public class Alien
    {
        public string Name { get; set; }
        public string Species { get; set; }
        public int HealthPoints { get; set; }

        public void Move()
        {
            // move
        }

        public void Attack()
        {
            // attack
        }

        public void Defend()
        {
            // defend
        }

        public void Run()
        {
            // run
        }

        public void Decide()
        {
            Random r = new Random();
            int num = r.Next(1, 10);
            if (isEven(num))
            {
                // even = engage
                num = r.Next(1, 10);
                if (isEven(num))
                {
                    Attack();
                }
                else
                {
                    Defend();
                }
            }
            else
            {
                // odd - flee
                Run();
            }

            bool isEven(int value)
            {
                return (value % 2 == 0);
            }
        }
    }
}