using System;

namespace BodiedMembers
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }

        private int _age;

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public Person(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            _age = age;
        }

        ~Person()
        {
            Console.WriteLine("Finalizing Person");
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}