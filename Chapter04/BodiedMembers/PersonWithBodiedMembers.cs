using System;

namespace BodiedMembers
{
    public class PersonWithBodiedMembers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";

        private int _age;

        public int Age
        {
            get => _age;
            set => _age = value;
        }

        public PersonWithBodiedMembers(string firstName, string lastName, int age) => init(firstName, lastName, age);
        
        private void init(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            _age = age;
        }

        ~PersonWithBodiedMembers() => Console.WriteLine("Finalizing Person with bodied members");

        public override string ToString() => FullName;
    }
}