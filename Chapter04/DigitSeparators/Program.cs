﻿using System;

namespace DigitSeparators
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 1750036;
            Console.WriteLine(number);
            number = 1_750_035;
            Console.WriteLine(number);
            double number2 = 1_750_036.49;
            Console.WriteLine(number2);
            int myBinary = 0b0010;
            Console.WriteLine(myBinary);
            myBinary = 0b1000_0010;
            Console.WriteLine(myBinary);
        }
    }
}