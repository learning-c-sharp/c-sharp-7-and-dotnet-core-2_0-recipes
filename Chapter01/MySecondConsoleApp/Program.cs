﻿using System;

namespace MySecondConsoleApp
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("What's your name?");
            string name = Console.ReadLine();
            Console.WriteLine("Hello, " + name);
            Console.ReadKey();
        }
    }
}