﻿using System;

namespace TypeChecking
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Person p = new Person("John", "Doe", 42);
            if (p is Person)
            {
                Console.WriteLine("p is a Person");
            }
            else
            {
                Console.WriteLine("p is not a Person");
            }
            Student s = new Student("Jane", "Doe", 24);
            if (s is Student)
            {
                Console.WriteLine("s is a Student");
            }
            else
            {
                Console.WriteLine("s is not a Student");
            }

            if (s is Person)
            {
                Console.WriteLine("s is a Person");
            }
            else
            {
                Console.WriteLine("s is not a Person");
            }
            
            if (p is Student)
            {
                Console.WriteLine("p is a Student");
            }
            else
            {
                Console.WriteLine("p is not a Student");
            }

            object obj = new Person("John", "Smith", 42);
            doSomething(obj);
        }

        private static void doSomething(object o)
        {
            Person p = o as Person;
            if (p == null)
            {
                throw new ArgumentException("Argument is not a Person");
            }

            Console.WriteLine(p);
        }
    }
}