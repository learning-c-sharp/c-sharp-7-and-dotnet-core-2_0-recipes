using System;

namespace TypeChecking
{
    public class Person
    {
        public static int MinAge = 18;
        public static int MaxAge = 70;
        
        private readonly string _firstName;
        private readonly string _lastName;

        public string FirstName => _firstName;

        public string LastName => _lastName;

        public int Age { get; set; }

        public Person(string firstName, string lastName, int age)
        {
            _firstName = firstName;
            _lastName = lastName;
            Age = age;
        }

        public virtual void SayHello()
        {
            Console.WriteLine("Hello, I'm a Person");
        }

        public override string ToString()
        {
            return FirstName + " " + LastName + " - age: " + Age;
        }
    }
}