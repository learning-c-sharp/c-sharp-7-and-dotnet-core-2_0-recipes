﻿using System;

namespace Structs
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Coordinates c = new Coordinates();
            c.Longitude = 42.67890;
            c.Latitude = 38.79851;
            Console.WriteLine("I'm here ({0}, {1})", c.Longitude, c.Latitude);
            
            changeCoordinates(c);
            Console.WriteLine("I'm here after ({0}, {1})", c.Longitude, c.Latitude);
        }

        public static void changeCoordinates(Coordinates c)
        {
            c.Longitude = 0;
            c.Latitude = 0;
        }
    }
}