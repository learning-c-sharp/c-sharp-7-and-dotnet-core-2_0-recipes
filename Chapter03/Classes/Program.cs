﻿using System;

namespace Classes
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Person person = new Person();
            person.FirstName = "John";
            person.LastName = "Smith";
            person.Age = 42;
            Console.WriteLine("{0} {1} - age: {2}", person.FirstName, person.LastName, person.Age);
            
            Person person2 = new Person("John", "Doe", 42);
            Console.WriteLine("{0} {1} - age: {2}", person2.FirstName, person2.LastName, person2.Age);
            
            PersonWithProps personWithProps = new PersonWithProps("Jane", "Doe", 32);
            Console.WriteLine("{0} {1} - age: {2}", personWithProps.FirstName, personWithProps.LastName, personWithProps.Age);
            personWithProps.Age = 33;
            Console.WriteLine("{0} {1} - age: {2}", personWithProps.FirstName, personWithProps.LastName, personWithProps.Age);

            personWithProps.Age = 17;
            if (personWithProps.Age < PersonWithProps.MinAge)
            {
                Console.WriteLine("too young");
            }

            Console.ReadKey();
        }
    }
}