namespace Classes
{
    public class PersonWithProps
    {
        public static int MinAge = 18;
        public static int MaxAge = 70;
        
        private readonly string _firstName;
        private readonly string _lastName;

        public string FirstName => _firstName;

        public string LastName => _lastName;

        public int Age { get; set; }

        private decimal _salary;

        public PersonWithProps(string firstName, string lastName, int age, decimal salary = 130000)
        {
            _firstName = firstName;
            _lastName = lastName;
            _salary = salary;
            Age = age;
        }
    }
}