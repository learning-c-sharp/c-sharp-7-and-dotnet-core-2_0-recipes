namespace Classes
{
    public class Person
    {
        public string FirstName;
        public string LastName;
        public int Age;

        private decimal salary;

        public Person() {}

        public Person(string firstName, string lastName, int age, decimal salary = 120000)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            
            this.salary = salary;
        }
    }
}