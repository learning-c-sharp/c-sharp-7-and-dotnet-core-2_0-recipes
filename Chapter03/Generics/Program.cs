﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Generics
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            List<string> games = new List<string>();
            games.Add("Civilization");
            games.Add("Star Wars: Old Republic");
            games.Add("Star Wars: Knights of the Old Republic");

            foreach (string game in games)
            {
                Console.WriteLine(game);
            }
            
            List<int> randomNumber = new List<int>();
            Random r = new Random();
            randomNumber.Add(r.Next(1, 10));
            randomNumber.Add(r.Next(1, 10));
            randomNumber.Add(r.Next(1, 10));
            foreach (int num in randomNumber)
            {
                Console.WriteLine(num);
            }
            
            MyStack<string> names = new MyStack<string>();
            names.Push("Eka");
            names.Push("Toka");
            names.Push("Kolmos");

            while (!names.IsEmpty())
            {
                Console.WriteLine(names.Pop());
            }
        }
    }
}