namespace Interfaces
{
    public interface IRun
    {
        void Run(int distance);
    }
}