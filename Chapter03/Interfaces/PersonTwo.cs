using System;

namespace Interfaces
{
    public class PersonTwo
    {
        public static int MinAge = 18;
        public static int MaxAge = 70;

        private IRun _runner;

        public string FirstName { get; }

        public string LastName { get; }

        public int Age { get; set; }

        public PersonTwo(string firstName, string lastName, int age, IRun runner)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            _runner = runner;
        }

        public virtual void SayHello()
        {
            Console.WriteLine("Hello, I'm a Person");
        }

        public override string ToString()
        {
            return FirstName + " " + LastName + " - age: " + Age;
        }

        public void Run(int distance)
        {
            _runner.Run(distance);
        }
    }
}