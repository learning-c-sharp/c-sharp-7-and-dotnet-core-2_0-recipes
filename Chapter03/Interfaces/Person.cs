using System;

namespace Interfaces
{
    public class Person : IRun
    {
        public static int MinAge = 18;
        public static int MaxAge = 70;

        public string FirstName { get; }

        public string LastName { get; }

        public int Age { get; set; }

        private decimal _salary;

        public Person(string firstName, string lastName, int age, decimal salary = 130000)
        {
            FirstName = firstName;
            LastName = lastName;
            _salary = salary;
            Age = age;
        }

        public virtual void SayHello()
        {
            Console.WriteLine("Hello, I'm a Person");
        }

        public override string ToString()
        {
            return FirstName + " " + LastName + " - age: " + Age;
        }

        public void Run(int distance)
        {
            Console.WriteLine("I am running for {0} km", distance);
        }
    }
}