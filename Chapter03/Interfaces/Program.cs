﻿using System;

namespace Interfaces
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Person person = new Person("John", "Smith", 42);
            Console.WriteLine(person);
            person.Run(10);
            
            PersonTwo fast = new PersonTwo("John", "Doe", 33, new FastRunner());
            PersonTwo slow = new PersonTwo("Jane", "Doe", 30, new SlowRunner());
            fast.Run(15);
            slow.Run(5);
            
            Dog dog = new Dog("Rex", new FastRunner());
            dog.Run(20);
        }
    }
}