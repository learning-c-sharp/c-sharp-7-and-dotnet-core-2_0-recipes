using System;

namespace Interfaces
{
    public class SlowRunner : IRun
    {
        public void Run(int distance)
        {
            Console.WriteLine("I run slowly for {0} km", distance);
        }
    }
}