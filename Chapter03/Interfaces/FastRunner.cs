using System;

namespace Interfaces
{
    public class FastRunner : IRun
    {
        public void Run(int distance)
        {
            Console.WriteLine("I run fast for {0} km", distance);
        }
    }
}