namespace Interfaces
{
    public class Dog
    {
        public string Name { get; set; }
        private IRun _runner;

        public Dog(string name, IRun runner)
        {
            _runner = runner;
            Name = name;
        }

        public void Run(int distance)
        {
            _runner.Run(distance);
        }
    }
}