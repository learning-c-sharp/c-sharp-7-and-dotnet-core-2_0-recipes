﻿using System;

namespace Inheritance
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Student student = new Student("John", "Smith", 21);
            student.University = "MIT";
            student.AvgGrade = 8;

            student.SayHello();
            Console.WriteLine(student);

            Console.ReadKey();
        }
    }
}