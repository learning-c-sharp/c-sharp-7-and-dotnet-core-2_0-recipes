﻿using System;
using System.Text;

namespace Methods
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            MyMethod();
            // method overloading
            PrintNumber(42);
            PrintNumber(1, 2);
            PrintNumber(1, 2, 3);
            
            // passing by reference vs by value
            double myDouble = 4.56;
            Console.WriteLine("myDouble = {0}", myDouble);
            IncrementDouble(myDouble);
            Console.WriteLine("myDouble in Main: {0}", myDouble);
            IncrementDoubleWithRef(ref myDouble);
            Console.WriteLine("myDouble in Main: {0}", myDouble);
            
            StringBuilder builder = new StringBuilder();
            builder.Append("Hello");
            builder.Append(" C#");
            Console.WriteLine("StringBuilder before: {0}", builder.ToString());
            AddExclamationMarks(builder);
            Console.WriteLine("StringBuilder after: {0}", builder.ToString());

            // returning values
            int a = 8;
            int b = 2;
            int sum, diff, prod;
            DoEverthing(a, b, out sum, out diff, out prod);
            Console.WriteLine("sum: {0}, diff: {1}, prod: {2}", sum, diff, prod);

            Console.ReadKey();
        }

        static void MyMethod()
        {
            Console.WriteLine("This is my new method");
        }

        static void PrintNumber(int num)
        {
            Console.WriteLine("Number: {0}", num);
        }

        static void PrintNumber(int a, int b)
        {
            Console.WriteLine(a + b);
        }

        static void PrintNumber(int a, int b, int c)
        {
            Console.WriteLine(a * b * c);
        }

        static void IncrementDouble(double d)
        {
            d++;
            Console.WriteLine("double in incrementDouble: {0}", d);
        }

        static void IncrementDoubleWithRef(ref double d)
        {
            d++;
            Console.WriteLine("double in incrementDoubleWithRef: {0}", d);
        }

        static void AddExclamationMarks(StringBuilder sb)
        {
            sb.Append("!!!");
        }

        static void DoEverthing(int a, int b, out int sum, out int diff, out int prod)
        {
            sum = a + b;
            diff = a - b;
            prod = a * b;
        }
    }
}