﻿using System;

namespace Enums
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Movie movie = new Movie("Star Wars", 1977, MovieGenre.Scifi);
            printMovieGenre(movie);
        }

        private static void printMovieGenre(Movie movie)
        {
            switch (movie.Genre)
            {
                case MovieGenre.Action:
                    Console.WriteLine("Action Movie");
                    break;
                case MovieGenre.Comedy:
                    Console.WriteLine("Comedy Movie");
                    break;
                case MovieGenre.Scifi:
                    Console.WriteLine("Science Fiction Movie");
                    break;
                case MovieGenre.Fantasy:
                    Console.WriteLine("Fantasy Movie");
                    break;
                default:
                    Console.WriteLine("Unknown Movie Genre");
                    break;
            }
        }
    }
}