namespace Enums
{
    public enum MovieGenre
    {
        Action,
        Comedy,
        Scifi,
        Fantasy
    }

    public class Movie
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public MovieGenre Genre { get; set; }

        public Movie(string title, int year, MovieGenre genre)
        {
            Title = title;
            Year = year;
            Genre = genre;
        }
    }
}