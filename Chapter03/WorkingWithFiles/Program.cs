﻿using System;
using System.IO;

namespace WorkingWithFiles
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            readFile();
            writeFile();
            readFileWithUsing();
            readFileAsync();
            readFileWithTry();
        }

        private static void readFile()
        {
            StreamReader reader = new StreamReader("../../test-file.txt");
            string contents = reader.ReadToEnd();
            Console.WriteLine(contents);
            reader.Close();
        }

        private static void readFileWithUsing()
        {
            using (StreamReader reader = new StreamReader("../../test-file.txt"))
            {
                string contents = reader.ReadToEnd();
                Console.WriteLine(contents);
            }
        }

        private static void readFileWithTry()
        {
            try
            {
                using (StreamReader reader = new StreamReader("non-existing-file.txt"))
                {
                    string contents = reader.ReadToEnd();
                    Console.WriteLine(contents);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found");
            }
            catch (FileLoadException e)
            {
                Console.WriteLine("File load failed");
            }
            catch (Exception)
            {
                Console.WriteLine("General exception");
            }
        }

        private static async void readFileAsync()
        {
            using (StreamReader reader = new StreamReader("../../test-file.txt"))
            {
                string contents = await reader.ReadToEndAsync();
                Console.WriteLine("Async file content:");
                Console.WriteLine(contents);
            }
        }

        private static void writeFile()
        {
            StreamWriter writer = new StreamWriter("file-test.txt", true);
            writer.WriteLine("Rider is very good IDE");
            writer.Close();
        }
    }
}