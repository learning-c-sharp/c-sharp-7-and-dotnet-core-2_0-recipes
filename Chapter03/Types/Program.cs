﻿using System;
using System.Text;

namespace Types
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int myInt = 42;
            Console.WriteLine("myInt = {0}", myInt);

            int a = 8;
            int b = 2;
            Console.WriteLine("{0} + {1} = {2}", a, b, a + b);

            char[] charArray = new char[] {'a', 'b', 'c'};
            Console.WriteLine("char array length {0}", charArray.Length);

            char[] secondArray = new char[4];
            Console.WriteLine("second array length {0}", secondArray.Length);

            secondArray[0] = 'b';
            secondArray[1] = 'a';
            secondArray[2] = 'l';
            secondArray[3] = 'k';

            for (int i = 0; i < secondArray.Length; i++)
            {
                Console.WriteLine(secondArray[i]);
            }

            Console.WriteLine("######");

            secondArray[2] = 'z';
            foreach (var t in secondArray)
            {
                Console.WriteLine(t);
            }

            string result = "";
            while (true)
            {
                Console.WriteLine("Type a word: ");
                string input = Console.ReadLine();

                if (input == "")
                {
                    break;
                }
                else
                {
                    result += input + " ";
                }
            }

            Console.WriteLine("Result: {0}", result);

            StringBuilder sb = new StringBuilder();
            while (true)
            {
                Console.WriteLine("Type a word: ");
                string input = Console.ReadLine();

                if (input == "")
                {
                    break;
                }
                else
                {
                    sb.Append(input).Append(" ");
                }
            }

            Console.WriteLine("Second Result: {0}", sb.ToString());

            Console.ReadKey();
        }
    }
}