using System.Collections.Generic;

namespace WebApplication1.Services
{
    public class FooService
    {
        public List<string> GetNames()
        {
            return new List<string>()
            {
                "Arno",
                "Lauri",
                "Alisa",
                "Karoliina"
            };
        }
    }
}